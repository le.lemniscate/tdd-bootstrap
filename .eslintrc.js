modules.exports = {
  "extends": "airbnb-base",
  "rules": {
    "import/no-extraneous-dependencies": ["error", { "devDependencies": ["**/test/**/*.js"] }],
  },
  "env": {
    "jest": true
  }
}
